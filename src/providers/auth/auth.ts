import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { Http } from '@angular/http';
export const googleWebClientId: string = '30928634249-ir5jkegs2sabn80rbc6vikqofn8lcnfu.apps.googleusercontent.com';

@Injectable()
export class AuthProvider {
  public fireAuth:firebase.auth.Auth;
  public ProfileRef:firebase.database.Reference;
  session: any;
  newUser: any = {};

  loggedin = false;

  constructor(public af: AngularFireAuth, public http: Http) {

    this.fireAuth = firebase.auth();
    this.ProfileRef = firebase.database().ref('/profiles');

    

  }

  login(email: string, password: string): Promise<any> {
    return this.fireAuth.signInWithEmailAndPassword(email, password);
  }

  loginFacebook() {
    return this.af.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
    .then((res) => {
      this.ProfileRef.child(res.user.uid).set({
        user_Mail: res.user.email,
        user_Name: res.user.displayName,
        user_Nickname: res.user.displayName,
        user_Tel: res.user.phoneNumber,
        user_Img: res.user.photoURL
      });
      this.newUser.user_No = res.user.uid;
        this.newUser.user_Mail = res.user.email;
        this.newUser.user_Tel = res.user.phoneNumber;
        this.newUser.user_Nickname = res.user.displayName;
        this.newUser.user_Name = res.user.displayName;
        this.newUser.action = "insert";
        this.http.post("http://15.164.118.95/user", this.newUser).
      subscribe(data => {
        let result = JSON.parse(data["_body"]);
        if (result.code == "100") {
          console.log("insert success");
        }
        else {
          console.log("fail");
        }
      });
      this.loggedin = true;

    });
  }

  loginGoogle() {
    return this.af.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
    .then((res) => {
      this.ProfileRef.child(res.user.uid).set({
        user_Mail: res.user.email,
        user_Name: res.user.displayName,
        user_Nickname: res.user.displayName,
        user_Tel: res.user.phoneNumber,
        user_Img: res.user.photoURL
      });
      console.log(JSON.stringify(res.user));
      this.newUser.user_No = res.user.uid;
        this.newUser.user_Mail = res.user.email;
        this.newUser.user_Tel = res.user.phoneNumber;
        this.newUser.user_Nickname = res.user.displayName;
        this.newUser.user_Name = res.user.displayName;
        this.newUser.action = "insert";
        this.http.post("http://15.164.118.95/user", this.newUser).
      subscribe(data => {
        let result = JSON.parse(data["_body"]);
        if (result.code == "100") {
          console.log("insert success");
        }
        else {
          console.log("fail");
        }
      });
      this.loggedin = true;

    });
  }

  
  

  register(email: string, password: string, username: string, nickname: string, phonenumber: string): Promise<any> {
    return this.fireAuth.createUserWithEmailAndPassword(email, password)
      .then(User => {
        this.ProfileRef.child(User.user.uid).set({
          user_Mail: email,
          user_Name: username,
          user_Nickname: nickname,
          user_Tel: phonenumber
        });
        this.newUser.user_No = User.user.uid;
        this.newUser.user_Mail = email;
        this.newUser.user_Tel = phonenumber;
        this.newUser.user_Nickname = username;
        this.newUser.user_Name = nickname;
        this.newUser.action = "insert";
        this.http.post("http://15.164.118.95/user", this.newUser).
      subscribe(data => {
        let result = JSON.parse(data["_body"]);
        if (result.code == "100") {
          console.log("insert success");
        }
        else {
          console.log("fail");
        }
      });
      });
  }

  resetPassword(email: string): Promise<void> {
    return this.fireAuth.sendPasswordResetEmail(email);
  }

  logoutUser(){
    this.fireAuth.signOut();
    this.loggedin = false;
  }

}
