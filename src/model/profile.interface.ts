export interface Profile {
    user_Mail: string;
    user_Name: string;
    user_Nickname: string;
    user_Tel: string;
    user_Img: string;
    user_No: any;
}