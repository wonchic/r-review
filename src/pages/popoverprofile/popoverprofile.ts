import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, AlertController, App } from 'ionic-angular';
import { AngularFireDatabase } from '@angular/fire/database';
import { LoginPage } from '../login/login';
import firebase from 'firebase';

@Component({
  templateUrl: 'popoverprofile.html',
})
export class PopoverprofilePage {
  uid: string ;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public app: App, 
    public navParams: NavParams, public alertCtrl: AlertController, public db: AngularFireDatabase) {
      this.uid = this.navParams.get('uid');
  }

  changeprofile() {
    this.navCtrl.push('UpdateprofilePage');
    this.viewCtrl.dismiss();
  }

  UserdeleteSelect(){
    let alert = this.alertCtrl.create({
      message: "회원탈퇴를 하시겠습니까?",
      buttons: [
        {
          text: "네",
          role: 'Ok',
          handler: () => { 
            this.viewCtrl.dismiss();
            this.Userdelete(); }
        },
        {
          text: "아니오",
          role: 'cancel',
          handler: () => { 
            this.viewCtrl.dismiss();
          }
        }
      ]
    });
    alert.present();
  }

  Userdelete(){
    var user = firebase.auth().currentUser;
    this.db.object(`profiles/${this.uid}`).remove();
    user.delete().then(()=> {
      this.navCtrl.setRoot(LoginPage);
    }
    );
   }



}
