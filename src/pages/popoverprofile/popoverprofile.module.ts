import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverprofilePage } from './popoverprofile';

@NgModule({
  declarations: [
    PopoverprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverprofilePage),
  ],
})
export class PopoverprofilePageModule {}
