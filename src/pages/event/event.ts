import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
@Component({
  selector: 'page-event',
  templateUrl: 'event.html'
})
export class EventPage {
  eventList:any;

  constructor(public navCtrl: NavController,public http: Http) {
    this.getData();

  }
  getData() {
    this.http.get("http://15.164.118.95/event").subscribe(data => {
      this.eventList = JSON.parse(data["_body"]);
      console.log(this.eventList);
    
    }, err => {
      console.log(err);
      alert(err);
    });
  }

}
