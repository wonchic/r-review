import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, Loading, AlertController} from 'ionic-angular';
import { TabsPage } from '../tabs/tabs'; 
import { AuthProvider } from '../../providers/auth/auth';
import { EmailValidator } from '../../validators/email';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  email: string;
  password: string;
  public loginForm: FormGroup;
  public loading: Loading;
  userProfile: any;


  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public alertCtrl: AlertController,
    public authProvider: AuthProvider, public formBuilder: FormBuilder) {
      this.loginForm = formBuilder.group({
        email: ['', Validators.compose([Validators.required,
          EmailValidator.isValid])],
        password: ['', Validators.compose([Validators.minLength(6),
          Validators.required])]
      });
  }
  

  login() {
    if (!this.loginForm.valid){
      console.log(this.loginForm.value);
    } else {
      this.authProvider.login(this.loginForm.value.email, this.loginForm.value.password)
        .then(() => {
        this.loading.dismiss().then( () => {
          alert("Login success");
          this.navCtrl.setRoot(TabsPage);
        });
      }, error => {
        this.loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      });
      this.loading = this.loadingCtrl.create();
      this.loading.present();
    }
  }

  loginFacebook() {
    this.authProvider.loginFacebook()
        .then(() => {
        this.loading.dismiss().then( () => {
          alert("Login success");
          this.navCtrl.setRoot(TabsPage);
      });
    }, error => {
      this.loading.dismiss().then( () => {
        let alert = this.alertCtrl.create({
          message: error.message,
          buttons: [
            {
              text: "Ok",
              role: 'cancel'
            }
          ]
        });
        alert.present();
      });
    });
    this.loading = this.loadingCtrl.create();
    this.loading.present();
  }

  loginGoogle(){
  this.authProvider.loginGoogle()
    .then(() => {
      this.loading.dismiss().then( () => {
        alert("Login success");
        this.navCtrl.setRoot(TabsPage);
      });
    }, error => {
      this.loading.dismiss().then( () => {
        let alert = this.alertCtrl.create({
          message: error.message,
          buttons: [
            {
              text: "Ok",
              role: 'cancel'
            }
          ]
        });
        alert.present();
      });
    });
  this.loading = this.loadingCtrl.create();
  this.loading.present();

  }

  goToResetPassword(): void {
    this.navCtrl.push('ResetPasswordPage');
  }

  register(): void {
    this.navCtrl.push('RegisterPage');
  }
}
