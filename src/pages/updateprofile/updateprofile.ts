import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, ActionSheetController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { Http } from '@angular/http';
import { Camera, CameraOptions} from '@ionic-native/camera';
import { Profile } from '../../model/profile.interface';
import firebase from 'firebase';


/**
 * Generated class for the UpdateprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-updateprofile',
  templateUrl: 'updateprofile.html',
})
export class UpdateprofilePage {
  updateprofile: Profile;
  profile: any;
  uid: string;
  public processString: string;
  private base64Prefix: string = 'data:image/jpeg;base64,';
  selectedPhoto;
  public ProfileRef:firebase.database.Reference;

  constructor(public navCtrl: NavController, public navParams: NavParams, private af: AngularFireAuth, public http: Http,
    private camera: Camera, private actionSheetController: ActionSheetController, public zone: NgZone) {
    this.uid = this.af.auth.currentUser.uid;
  }

  ionViewDidLoad() {
    this.getData();
  }

  getData() {
    this.http.get("http://15.164.118.95/user/" + this.uid).subscribe(data => {
      this.profile = JSON.parse(data["_body"]);
      console.log(this.profile.user_Img);
    }, err => {
      console.log(err);
    });
  }

  updateData(){
    this.http.post("http://15.164.118.95/user", this.profile).
      subscribe(data => {
        let result = JSON.parse(data["_body"]);
        if (result.code == "100") {
          console.log("insert success");
        }
        else {
          console.log("fail");
        }
      });
      this.navCtrl.pop();
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      title: "Select Image source",
      buttons: [
        {
          text: '앨범에서 사진 선택',
          handler: () => {
            this.choosePhoto();
            
          }
        },
        {
          text: '사진 촬영',
          handler: () => {
            this.takePicture();

          }
        },
        {
          text: '기본 이미지로 변경',
          handler: () => {
            this.deletePhoto();

          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    await actionSheet.present();
  }

  choosePhoto() {
    const options: CameraOptions = {
      quality: 50,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then((imageData) => {

      this.selectedPhoto  = this.dataURItoBlob(this.base64Prefix + imageData);
      this.upload();
    }, (err) => {
      console.log(err);
    });

  }

  takePicture() {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
      // saveToPhotoAlbum: false,
      // correctOrientation: true

    }
    this.camera.getPicture(options).then((imageData) => {
      this.selectedPhoto  = this.dataURItoBlob(this.base64Prefix + imageData);
      this.upload();
    }, (err) => {
      console.log(err);
    });
  }

  deletePhoto() {
    this.profile.user_Img = null;
    this.http.post("http://15.164.118.95/user", this.profile).subscribe(data => {
                let result = JSON.parse(data["_body"]);
                  if (result.code == "100") {
      
                    }
              else {
                }
            });
  }

  dataURItoBlob(dataURI) {
        let binary = atob(dataURI.split(',')[1]);
        let array = [];
        for (let i = 0; i < binary.length; i++) {
          array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {type: 'image/jpeg'});
      }

      upload() {
        if (this.selectedPhoto) {
          
          var uploadTask = firebase.storage().ref().child(`images/${this.uid}.png`)
            .put(this.selectedPhoto);
          uploadTask.then(this.onSuccess, this.onError);
        }
      }

      onSuccess = snapshot => {
        firebase.storage().ref().child(`images/${this.uid}.png`).getDownloadURL().then((url) => {
          this.zone.run(() => {
            this.profile.user_Img = url;
            this.http.post("http://15.164.118.95/user", this.profile).subscribe(data => {
                let result = JSON.parse(data["_body"]);
                  if (result.code == "100") {
      
                    }
              else {
                }
            });

           })
        })
      };
      
      onError = error => {
        console.log("error", error);
        
      };

}
