import { Component, NgZone } from '@angular/core';
import { NavController, NavParams, ActionSheetController, AlertController, App, PopoverController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from '@angular/fire/database'; 
import { LoginPage } from '../login/login';
import { PopoverprofilePage } from '../popoverprofile/popoverprofile';
import { Http } from '@angular/http';
import { Camera, CameraOptions} from '@ionic-native/camera';
import { AuthProvider } from '../../providers/auth/auth';
import firebase from 'firebase';


@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {
  profile: any;
  uid: string;
  auth: AngularFireAuth;
  subscription: any;

  public processString: string;
  private base64Prefix: string = 'data:image/jpeg;base64,';
  selectedPhoto;
  public ProfileRef:firebase.database.Reference;
  


  constructor(public navCtrl: NavController, public http: Http, public navParams: NavParams,
    private af: AngularFireAuth, public authProvider: AuthProvider, public alertCtrl: AlertController, public db: AngularFireDatabase,
    private app: App, private camera: Camera, private actionSheetController: ActionSheetController, public zone: NgZone, public popoverCtrl: PopoverController) {
    if (this.af.auth.currentUser) {
      this.uid = this.af.auth.currentUser.uid;
      this.ProfileRef = firebase.database().ref('/profiles');
      console.log(this.af.auth.currentUser.uid);
    } else {
      console.log('생성자 ) 로그인 데이터 없음');
      this.uid = null;
    }
  }

  ionViewDidLoad() {
    this.getData();
  }
  getData() {
    this.http.get("http://15.164.118.95/user/" + this.uid).subscribe(data => {
      this.profile = JSON.parse(data["_body"]);
      console.log(this.profile.user_Img);
    }, err => {
      console.log(err);
    });
  }

  async logout() {
    await this.authProvider.logoutUser();
    this.app.getRootNav().setRoot(LoginPage);
  }

  moveLogin() {
    this.navCtrl.setRoot(LoginPage);
  }
  
  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      title: "Select Image source",
      buttons: [
        {
          text: '앨범에서 사진 선택',
          handler: () => {
            this.choosePhoto();
            
          }
        },
        {
          text: '사진 촬영',
          handler: () => {
            this.takePicture();

          }
        },
        {
          text: '기본 이미지로 변경',
          handler: () => {
            this.deletePhoto();

          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    await actionSheet.present();
  }

  choosePhoto() {
    const options: CameraOptions = {
      quality: 50,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then((imageData) => {

      this.selectedPhoto  = this.dataURItoBlob(this.base64Prefix + imageData);
      this.upload();
    }, (err) => {
      console.log(err);
    });

  }

  takePicture() {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE

    }
    this.camera.getPicture(options).then((imageData) => {
      this.selectedPhoto  = this.dataURItoBlob(this.base64Prefix + imageData);
      this.upload();
    }, (err) => {
      console.log(err);
    });
  }

  deletePhoto() {
    this.profile.user_Img = null;
    this.http.post("http://15.164.118.95/user", this.profile).subscribe(data => {
                let result = JSON.parse(data["_body"]);
                  if (result.code == "100") {
      
                    }
              else {
                }
            });
  }

  dataURItoBlob(dataURI) {
        let binary = atob(dataURI.split(',')[1]);
        let array = [];
        for (let i = 0; i < binary.length; i++) {
          array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {type: 'image/jpeg'});
      }

      upload() {
        if (this.selectedPhoto) {
          
          var uploadTask = firebase.storage().ref().child(`images/${this.uid}.png`)
            .put(this.selectedPhoto);
          uploadTask.then(this.onSuccess, this.onError);
        }
      }

      onSuccess = snapshot => {
        firebase.storage().ref().child(`images/${this.uid}.png`).getDownloadURL().then((url) => {
          this.zone.run(() => {
            this.profile.user_Img = url;
            this.http.post("http://15.164.118.95/user", this.profile).subscribe(data => {
                let result = JSON.parse(data["_body"]);
                  if (result.code == "100") {
      
                    }
              else {
                }
            });

           })
        })
      };
      
      onError = error => {
        console.log("error", error);
        
      };

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverprofilePage,{uid : this.uid});
    popover.present({
      ev: myEvent
    });
  }
}
