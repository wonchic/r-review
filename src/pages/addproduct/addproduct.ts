import { Component, NgZone } from '@angular/core';
import { NavController, ToastController, Events, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { AngularFireAuth } from 'angularfire2/auth';
import { ImagePicker } from '@ionic-native/image-picker';
import firebase from 'firebase';

import Web3 from 'web3';
import Tx from 'ethereumjs-tx';

@Component({
   selector: 'page-addproduct',
   templateUrl: 'addproduct.html'
})
export class Addproduct {
   product: any = {};
   photos: any;
   photopreview: any;
   photosurl: any;
   private base64Prefix: string = 'data:image/jpeg;base64,';

   //블록체인
   web3: any;
   ReviewContract: any;

   constructor(public navCtrl: NavController, public http: Http, public toast: ToastController,
      private af: AngularFireAuth, private alertCtrl: AlertController, private imagePicker: ImagePicker,
      public zone: NgZone, public events: Events) {
      this.product.review_Category = null;
      this.product.review_Title = null;
      this.product.review_Content = null;
      this.product.review_Img = "";
      this.product.review_Satisfaction = 3;
      this.photos = [];
      this.photopreview = [];
      this.photosurl = [];
      this.events.subscribe('star-rating:changed', (starRating) => {
         this.product.review_Satisfaction = starRating;
         console.log(this.product.review_Satisfaction);
      });


      //블록체인




   }
   blockchain(No, Sa) {
      if (typeof this.web3 !== 'undefined') {
         this.web3 = new Web3(this.web3.currentProvider);
      } else {
         // set the provider you want from Web3.providers
         this.web3 = new Web3(new Web3.providers.WebsocketProvider('wss://ropsten.infura.io/ws'));
      }
      this.ReviewContract = new this.web3.eth.Contract([
         {
            "constant": true,
            "inputs": [],
            "name": "name",
            "outputs": [
               {
                  "name": "",
                  "type": "string"
               }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
         },
         {
            "constant": false,
            "inputs": [
               {
                  "name": "spender",
                  "type": "address"
               },
               {
                  "name": "value",
                  "type": "uint256"
               }
            ],
            "name": "approve",
            "outputs": [
               {
                  "name": "",
                  "type": "bool"
               }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
         },
         {
            "constant": true,
            "inputs": [],
            "name": "totalSupply",
            "outputs": [
               {
                  "name": "",
                  "type": "uint256"
               }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
         },
         {
            "constant": false,
            "inputs": [
               {
                  "name": "sender",
                  "type": "address"
               },
               {
                  "name": "recipient",
                  "type": "address"
               },
               {
                  "name": "amount",
                  "type": "uint256"
               }
            ],
            "name": "transferFrom",
            "outputs": [
               {
                  "name": "",
                  "type": "bool"
               }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
         },
         {
            "constant": true,
            "inputs": [],
            "name": "DECIMALS",
            "outputs": [
               {
                  "name": "",
                  "type": "uint8"
               }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
         },
         {
            "constant": true,
            "inputs": [],
            "name": "INITIAL_SUPPLY",
            "outputs": [
               {
                  "name": "",
                  "type": "uint256"
               }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
         },
         {
            "constant": true,
            "inputs": [],
            "name": "decimals",
            "outputs": [
               {
                  "name": "",
                  "type": "uint8"
               }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
         },
         {
            "constant": false,
            "inputs": [
               {
                  "name": "spender",
                  "type": "address"
               },
               {
                  "name": "addedValue",
                  "type": "uint256"
               }
            ],
            "name": "increaseAllowance",
            "outputs": [
               {
                  "name": "",
                  "type": "bool"
               }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
         },
         {
            "constant": true,
            "inputs": [],
            "name": "getInstructor",
            "outputs": [
               {
                  "name": "",
                  "type": "uint256"
               },
               {
                  "name": "",
                  "type": "uint256"
               }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
         },
         {
            "constant": false,
            "inputs": [
               {
                  "name": "amount",
                  "type": "uint256"
               }
            ],
            "name": "burn",
            "outputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
         },
         {
            "constant": true,
            "inputs": [
               {
                  "name": "account",
                  "type": "address"
               }
            ],
            "name": "balanceOf",
            "outputs": [
               {
                  "name": "",
                  "type": "uint256"
               }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
         },
         {
            "constant": false,
            "inputs": [
               {
                  "name": "account",
                  "type": "address"
               },
               {
                  "name": "amount",
                  "type": "uint256"
               }
            ],
            "name": "burnFrom",
            "outputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
         },
         {
            "constant": true,
            "inputs": [],
            "name": "symbol",
            "outputs": [
               {
                  "name": "",
                  "type": "string"
               }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
         },
         {
            "constant": false,
            "inputs": [
               {
                  "name": "spender",
                  "type": "address"
               },
               {
                  "name": "subtractedValue",
                  "type": "uint256"
               }
            ],
            "name": "decreaseAllowance",
            "outputs": [
               {
                  "name": "",
                  "type": "bool"
               }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
         },
         {
            "constant": false,
            "inputs": [
               {
                  "name": "recipient",
                  "type": "address"
               },
               {
                  "name": "amount",
                  "type": "uint256"
               }
            ],
            "name": "transfer",
            "outputs": [
               {
                  "name": "",
                  "type": "bool"
               }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
         },
         {
            "constant": true,
            "inputs": [
               {
                  "name": "owner",
                  "type": "address"
               },
               {
                  "name": "spender",
                  "type": "address"
               }
            ],
            "name": "allowance",
            "outputs": [
               {
                  "name": "",
                  "type": "uint256"
               }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
         },
         {
            "constant": false,
            "inputs": [
               {
                  "name": "_satisfaction",
                  "type": "uint256"
               },
               {
                  "name": "_reviewNo",
                  "type": "uint256"
               }
            ],
            "name": "setInstructor",
            "outputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
         },
         {
            "inputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "constructor"
         },
         {
            "anonymous": false,
            "inputs": [
               {
                  "indexed": true,
                  "name": "from",
                  "type": "address"
               },
               {
                  "indexed": true,
                  "name": "to",
                  "type": "address"
               },
               {
                  "indexed": false,
                  "name": "value",
                  "type": "uint256"
               }
            ],
            "name": "Transfer",
            "type": "event"
         },
         {
            "anonymous": false,
            "inputs": [
               {
                  "indexed": true,
                  "name": "owner",
                  "type": "address"
               },
               {
                  "indexed": true,
                  "name": "spender",
                  "type": "address"
               },
               {
                  "indexed": false,
                  "name": "value",
                  "type": "uint256"
               }
            ],
            "name": "Approval",
            "type": "event"
         }
      ], '0x1ff368d360c97e213c412db90998a3cc43776086');

      //console.log(No,Sa);
      const send_account = "0x525a44a7e1971d0568c86f9314e1f8d302d7d472";
      const privateKey = Buffer.from('A1267693342D34053D137E78394A3893B99DE1C988FBEE2B37E4F989F54E47AD', 'hex');
      this.web3.eth.getTransactionCount(send_account, (err, txCount) => {

         const txObject = {
            nonce: this.web3.utils.toHex(txCount),
            gasLimit: this.web3.utils.toHex(3000000), // Raise the gas limit to a much higher amount
            gasPrice: this.web3.utils.toHex(this.web3.utils.toWei('10', 'gwei')),
            to: '0x1ff368d360c97e213c412db90998a3cc43776086',
            data: this.ReviewContract.methods.setInstructor(No, Sa).encodeABI()
         };

         const tx = new Tx(txObject);
         tx.sign(privateKey);

         const serializedTx = tx.serialize();
         const raw = '0x' + serializedTx.toString('hex');

         this.web3.eth.sendSignedTransaction(raw)
            .once('transactionHash', (hash) => {
               console.info('transactionHash', 'https://ropsten.etherscan.io/tx/' + hash);
            })
            .once('receipt', (receipt) => {
               console.info('receipt', receipt);
               this.ReviewContract.methods.getInstructor().call().then(result => console.log(result));
            }).on('error', console.error);
      });
   }




   ionViewDidLoad() {
      console.log('Hello Addproduct Page');

   }
   insert() {
      if (this.product.review_Title != null) {
         if (this.product.review_Category != null) {
            if (this.product.review_Img != ""){
            this.product.user_No = this.af.auth.currentUser.uid;
            this.product.action = "insert";
            console.log(this.product);
            this.http.post("http://15.164.118.95/review", this.product).
               subscribe(data => {
                  let result = JSON.parse(data["_body"]);
                  console.log(result);
                  this.showToast("Inserted successfully");
                  this.blockchain(result.review_No, result.review_Satisfaction);
                  this.navCtrl.pop();

               }, err => {
                  console.log(err);
               });

             for (var i = 0; i < this.photos.length; i++) {
                 this.upload(this.photos[i]);
             }
              } else {
                     this.showToast("사진을 넣어주세요");
                 }
         } else {
            this.showToast("카테고리를 입력하세요");
         }
      } else {
         this.showToast("제목을 입력하세요");
      }
   }

   showToast(message) {
      let toast = this.toast.create({
         message: message,
         duration: 2000
      });
      toast.present();
   }

   saveUserImageFromGallery() {
      let options = {
         maximumImagesCount: 3,
         width: 800,
         height: 800,
         quality: 50,
         outputType: 1//image uri
      };
      this.imagePicker.getPictures(options).then((upload_file) => {
         for (var i = 0; i < upload_file.length; i++) {
            this.photos[i] = this.dataURItoBlob(this.base64Prefix + upload_file[i]);
            this.photopreview.push(this.base64Prefix + upload_file[i]);
            this.upload(this.photos[i]);
         }
      }, () => {
         console.log('Error picking image');
      });

   }

   dataURItoBlob(dataURI) {
      let binary = atob(dataURI.split(',')[1]);
      let array = [];
      for (let i = 0; i < binary.length; i++) {
         array.push(binary.charCodeAt(i));
      }
      return new Blob([new Uint8Array(array)], { type: 'image/png' });
   }



   deletePhoto(index) {
      let confirm = this.alertCtrl.create({
         title: 'Sure you want to delete this photo? There is NO undo!',
         message: '',
         buttons: [
            {
               text: 'No',
               handler: () => {
                  console.log('Disagree clicked');
               }
            }, {
               text: 'Yes',
               handler: () => {
                  console.log('Agree clicked');
                  this.photos.splice(index, 1);
                  this.photosurl.splice(index, 1);
                  this.photopreview.splice(index, 1);
                  this.product.review_Img = "";
                  for (var i = 0; i < this.photosurl.length; i++) {
                     this.product.review_Img += this.photosurl[i] + ",";
                  }

               }
            }
         ]
      });
      confirm.present();
   }

   upload(imageURI) {
      if (imageURI) {

         firebase.storage().ref().child(`image/${this.af.auth.currentUser.uid}/${Date.now()}.png`)
            .put(imageURI).then(
               (res) => {
                  firebase.storage().refFromURL(`${res.ref}`).getDownloadURL().then((url) => {
                     this.photosurl.push(url);
                     this.product.review_Img += url + ",";
                  });

               });

      }
   }

}