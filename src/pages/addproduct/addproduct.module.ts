import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Addproduct } from './addproduct';

@NgModule({
  declarations: [
    Addproduct,
  ],
  imports: [
    IonicPageModule.forChild(Addproduct),
  ],
})
export class AddproductPageModule {}
