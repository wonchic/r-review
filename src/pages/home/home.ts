import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { Http } from '@angular/http';
import { AngularFireAuth } from 'angularfire2/auth';
import { ContentPage } from '../content/content';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  eventList:any;
  bestProductList:any;
  img:any;
  uid:any;
  bestreviewphoto:any;

  constructor(public navCtrl: NavController,public http: Http,private af: AngularFireAuth) {
    if (this.af.auth.currentUser) {
      this.uid = this.af.auth.currentUser.uid;
      console.log('생성자 ) 로그인 데이터있음');
    } else {
      console.log('생성자 ) 로그인 데이터 없음');
      this.uid = null;
    }
    this.img = [];
    this.getData();

  }


  getData() {
    this.http.get("http://15.164.118.95/review/best").subscribe(data => {
      this.bestProductList = JSON.parse(data["_body"]);
      var str = this.bestProductList.review_Img;
      if(str){
        this.bestProductList.review_Img = str.split(",");
        //this.img.push(this.bestProductList.review_Img[0]);
         console.log(this.bestProductList.review_Img);
      }
    
    }, err => {
      console.log(err);
      alert(err);
    });

    this.http.get("http://15.164.118.95/event").subscribe(data => {
      this.eventList = JSON.parse(data["_body"]);
      console.log(this.eventList);
    
    }, err => {
      console.log(err);
      alert(err);
    });
  }
  moveToContent(data) {
    console.log(data.review_No);
    this.navCtrl.push(ContentPage, { contentNo: data.review_No});
  }


  moveToProfile(){
    this.navCtrl.push(ProfilePage);
  }



}
