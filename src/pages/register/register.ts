import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { EmailValidator } from '../../validators/email';
import { PhoneValidator } from '../../validators/phonenumber';
import { Http } from '@angular/http';
import { AngularFireAuth } from 'angularfire2/auth';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  public registerForm: FormGroup;
  public loading: Loading;
  newUser: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public authProvider: AuthProvider, public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController, public http: Http, public alertCtrl: AlertController, private af: AngularFireAuth) {

    this.registerForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required,
      EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6),
      Validators.required])],
      userName: ['', Validators.compose([Validators.minLength(2),
      Validators.required])],
      nickName: ['', Validators.compose([Validators.minLength(1),
      Validators.required])],
      phonenumber: ['', Validators.compose([Validators.required,
      PhoneValidator.isValid])]
    });

  }

  register() {
    if (!this.registerForm.valid) {
      console.log(this.registerForm.value);
    } else {
      this.authProvider.register(this.registerForm.value.email,
        this.registerForm.value.password,
        this.registerForm.value.userName,
        this.registerForm.value.nickName,
        this.registerForm.value.phonenumber)
        .then(() => {
          this.loading.dismiss().then(() => {
            this.myRegister();
            this.navCtrl.pop();
          });
        }, (error) => {
          this.loading.dismiss().then(() => {
            let alert = this.alertCtrl.create({
              message: error.message,
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            });
            alert.present();
          });
        });
      this.loading = this.loadingCtrl.create();
      this.loading.present();
    }
  }
  myRegister() {
    this.newUser.user_No = this.af.auth.currentUser.uid;
    this.newUser.user_Mail = this.registerForm.value.email;
    this.newUser.user_Tel = this.registerForm.value.phonenumber;
    this.newUser.user_Nickname = this.registerForm.value.nickName;
    this.newUser.user_Name = this.registerForm.value.userName;
    this.newUser.action = "insert";
    console.log(this.newUser);
    this.http.post("http://15.164.118.95/user", this.newUser).
      subscribe(data => {
        let result = JSON.parse(data["_body"]);
        if (result.code == "100") {
          console.log("insert success");
        }
        else {
          console.log("fail");
        }
      });
  }
}
