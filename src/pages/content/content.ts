import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import { AngularFireAuth } from 'angularfire2/auth';
/**
 * Generated class for the ContentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-content',
  templateUrl: 'content.html',
})
export class ContentPage {
  product: any;
  commentList: any;
  newcomment: any = {};
  reviewNo: any = {};
  photos: any;
  uid: string;
  isClicked: boolean;

  constructor(public navCtrl: NavController,
    public navParams: NavParams, public http: Http, public toast: ToastController,
    private af: AngularFireAuth, public platform: Platform) {

   
    this.newcomment.review_No = this.reviewNo;
    this.photos = [];
    //this.newcomment.comment_Content = null;
 
    this.reviewNo = navParams.get('contentNo');


    if (this.af.auth.currentUser) {
      this.uid = this.af.auth.currentUser.uid;
      console.log('생성자 ) 로그인 데이터있음' + this.uid);
    } else {
      console.log('생성자 ) 로그인 데이터 없음');
      this.uid = null;
    }
  }

  ionViewDidLoad() {
    this.getReview();
  }

  getReview() {
    this.newcomment.comment_Content = null;
    this.http.get("http://15.164.118.95/review/content/" + this.reviewNo).subscribe(data => {
      this.product = JSON.parse(data["_body"]);
      var str = this.product.review_Img;
      this.product.review_Img = str.split(",");
      for (var i = 0; i < this.product.review_Img.length - 1; i++){
        this.photos.push(this.product.review_Img[i]);
      }
    }, err => {
      console.log(err);
    });

    this.http.get("http://15.164.118.95/review/content/comment/" + this.reviewNo).subscribe(data => {
      this.commentList = JSON.parse(data["_body"]);
      console.log(this.commentList);
    }, err => {
      console.log(err);
    });
  }
  getData() {
    this.newcomment.comment_Content = null;
    this.http.get("http://15.164.118.95/review/content/" + this.reviewNo).subscribe(data => {
      this.product = JSON.parse(data["_body"]);
    }, err => {
      console.log(err);
    });
    this.http.get("http://15.164.118.95/review/content/comment/" + this.reviewNo).subscribe(data => {
      this.commentList = JSON.parse(data["_body"]);
      console.log(this.commentList);
    }, err => {
      console.log(err);
    });
  }
  insertComment() {
    if (this.uid != null) {
      if (this.newcomment.comment_Content != null) {
        this.newcomment.user_No = this.af.auth.currentUser.uid;
        this.newcomment.action = "insert";
        this.newcomment.review_No=this.reviewNo;
        console.log(this.newcomment);
        this.http.post("http://15.164.118.95/comment", this.newcomment).
          subscribe(data => {
            let result = JSON.parse(data["_body"]);
            if (result.code == "100") {
              this.showToast("Inserted successfully");
              this.getData();
            }
            else {
              this.showToast("Something went wrong");
            }
          }, err => {
            console.log(err);
          });

      } else {
        this.showToast("내용을 입력해주세요.");
      }
    } else {
      this.showToast("로그인이 필요한 서비스 입니다.");
    }

  }
  insertLikeC(comment) {
    if (this.uid != null) {
      comment.user_No = this.af.auth.currentUser.uid;
      comment.action = "insert";
      this.http.post("http://15.164.118.95/comment/like", comment).
        subscribe(data => {
          let result = JSON.parse(data["_body"]);
          if (result.code == "100") {
            this.showToast("Inserted successfully");
            this.getData();
            this.isClicked = true;
          }
          else {
            this.showToast("Something went wrong");
          }
        }, err => {
          console.log(err);
        });
    } else {
      this.showToast("로그인이 필요한 서비스 입니다.");
    }

  }
  insertLikeR(product) {
    if (this.uid != null) {
      product.user_No = this.af.auth.currentUser.uid;
      product.action = "insert";
      this.http.post("http://15.164.118.95/review/like", product).
        subscribe(data => {
          let result = JSON.parse(data["_body"]);
          if (result.code == "100") {
            this.showToast("Inserted successfully");
            this.getData();
          }
          else {
            this.showToast("Something went wrong");
          }
        }, err => {
          console.log(err);
        });
    } else {
      this.showToast("로그인이 필요한 서비스 입니다.");
    }

  }
  deleteR(product) {
    product.action = "insert";
    this.http.post("http://15.164.118.95/reviewdelete", product).
      subscribe(data => {
        let result = JSON.parse(data["_body"]);
        if (result.code == "100") {
          this.showToast("Inserted successfully");
          this.navCtrl.pop();
        }
        else {
          this.showToast("Something went wrong");
        }
      }, err => {
        console.log(err);
      });
  }

  deleteC(comment) {
    comment.action = "insert";
    this.http.post("http://15.164.118.95/commentdelete", comment).
      subscribe(data => {
        let result = JSON.parse(data["_body"]);
        if (result.code == "100") {
          this.showToast("Inserted successfully");
          this.navCtrl.pop();
        }
        else {
          this.showToast("Something went wrong");
        }
      }, err => {
        console.log(err);
      });
  }

  showToast(message) {
    let toast = this.toast.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
  

}
