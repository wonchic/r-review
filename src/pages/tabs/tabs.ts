import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EventPage } from '../event/event';
import { HomePage } from '../home/home';
import { ReviewPage } from '../review/review';
import { AngularFireAuth } from 'angularfire2/auth';
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  homeRoot = HomePage;
  reviewRoot = ReviewPage;
  eventRoot = EventPage;
  uid: string ;

  constructor(public navCtrl: NavController, public navParams: NavParams,private af: AngularFireAuth) {
    
    if(this.uid = navParams.get('uid')){

    }else{
      this.uid=this.af.auth.currentUser.uid;
    }
    console.log(this.uid);
  }
}
