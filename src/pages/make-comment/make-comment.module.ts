import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MakeCommentPage } from './make-comment';

@NgModule({
  declarations: [
    MakeCommentPage,
  ],
  imports: [
    IonicPageModule.forChild(MakeCommentPage),
  ],
})
export class MakeCommentPageModule {}
