import { Component } from '@angular/core';
import { NavController, AlertController, ToastController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { Addproduct } from '../addproduct/addproduct';
import { ContentPage } from '../content/content';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'page-review',
  templateUrl: 'review.html',
})
export class ReviewPage {
  productList: any;
  uid: string;
  photos: any;
  search_Content:any;



  constructor(public navCtrl: NavController, public http: Http,
    private af: AngularFireAuth, public navParams: NavParams, public alertCtrl: AlertController, public toast: ToastController) {
    if (this.af.auth.currentUser) {
      this.uid = this.af.auth.currentUser.uid;
      console.log('생성자 ) 로그인 데이터있음');
    } else {
      console.log('생성자 ) 로그인 데이터 없음');
      this.uid = null;
    }
    this.photos = [];


  }
  ionViewDidLoad() {
    this.getData();
  }

  getData() {
    this.http.get("http://15.164.118.95/test").subscribe(data => {
      this.productList = JSON.parse(data["_body"]);
      console.log(this.productList.review_No);
      console.log(this.productList);
      console.log(this.productList.review_Img);
      var str = this.productList.review_Img;
      if(str){
        this.productList.review_Img = str.split(",");
        for (var i = 0; i < this.productList.review_Img.length - 1; i++) {
        this.photos.push(this.productList.review_Img[i]);
        }
         console.log(str);
      }

    
    }, err => {
      console.log(err);
      alert(err);
    });
  }

  showCategory(category) {
    console.log(category);
    this.http.get("http://15.164.118.95/review/" + category).subscribe(data => {
      this.productList = JSON.parse(data["_body"]);
      console.log(this.productList);
      console.log(this.productList.review_Img);
      var str = this.productList.review_Img;
      if(str){
        this.productList.review_Img = str.split(",");
        for (var i = 0; i < this.productList.review_Img.length - 1; i++) {
        this.photos.push(this.productList.review_Img[i]);
        }
         console.log(str);
      }
    
    }, err => {
      console.log(err);
      alert(err);
    });
  }

  submitSearch(event){
    console.log(event);
    if(!event){

    }else{
      this.http.get("http://15.164.118.95/search/"+event).subscribe(data => {
      this.productList = JSON.parse(data["_body"]);
      console.log(this.productList);
    
    }, err => {
      console.log(err);
      alert(err);
    });
    }
    
  }



  moveToMakeR() {
    if (this.uid == null) {
      this.showToast("로그인이 필요한 서비스 입니다.");
    } else {
      this.navCtrl.push(Addproduct);
    }
  }
  moveToContent(data) {
    console.log(data.review_No);
    this.navCtrl.push(ContentPage, { contentNo: data.review_No, uid: this.uid });
  }


  Confirm(Message, Action) {
    this.alertCtrl.create({
      title: Message,
      buttons: [
        { text: 'Cancel' },
        {
          text: 'Sure',
          handler: Action
        }
      ]
    }).present();
  }

  presentAlertConfirm() {
    let alert = this.alertCtrl.create({
      title: '로그인이 필요한 서비스입니다.',
      message: 'Please confirm ...',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: '로그인',
          handler: () => {
            this.navCtrl.setRoot('LoginPage');
          }
        }
      ]
    });

    alert.present();
  }
  showToast(message) {
    let toast = this.toast.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}