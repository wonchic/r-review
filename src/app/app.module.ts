import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database'; 
import {AngularFireAuthModule} from 'angularfire2/auth';
import { HttpModule} from '@angular/http'; 
import { ProfilePage } from '../pages/profile/profile';
import { EventPage } from '../pages/event/event';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ReviewPage } from '../pages/review/review';
import { LoginPage } from '../pages/login/login';
import { WelcomePage } from '../pages/welcome/welcome';
import { PopoverprofilePage } from '../pages/popoverprofile/popoverprofile';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';
import { Addproduct } from '../pages/addproduct/addproduct';
import { ContentPage } from '../pages/content/content'
import { MakeCommentPage } from '../pages/make-comment/make-comment'
import { AuthProvider } from '../providers/auth/auth';
import { EventProvider } from '../providers/event/event';
import { ProfileProvider } from '../providers/profile/profile';
import { Transfer } from '@ionic-native/transfer';
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64';
import { Crop } from '@ionic-native/crop';
import { ImageResizer } from '@ionic-native/image-resizer';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { StarRatingModule } from 'ionic3-star-rating';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  declarations: [
    MyApp,
    WelcomePage,
    ProfilePage,
    EventPage,
    HomePage,
    TabsPage,
    ReviewPage,
    Addproduct,
    ContentPage,
    MakeCommentPage,
    LoginPage,
    PopoverprofilePage
  ],
  imports: [
    NgxPaginationModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyB6eTKNavkomPyG8FRsoLYdyDBBUSfuTLw",
      authDomain: "r-review-e3cf7.firebaseapp.com",
      databaseURL: "https://r-review-e3cf7.firebaseio.com",
      projectId: "r-review-e3cf7",
      storageBucket: "r-review-e3cf7.appspot.com",
      messagingSenderId: "593909550018"}),
      AngularFireDatabaseModule,
      AngularFireAuthModule,
      HttpModule,
      StarRatingModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    WelcomePage,
    ProfilePage,
    EventPage,
    HomePage,
    TabsPage,
    ReviewPage,
    Addproduct,
    ContentPage,
    MakeCommentPage,
    LoginPage,
    PopoverprofilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    Transfer,
    ImagePicker,
    Base64,
    Crop,
    ImageResizer,
    FileTransfer,
    File,

    AngularFireDatabase,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    EventProvider,
    ProfileProvider
  ]
})
export class AppModule {}