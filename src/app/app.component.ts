import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';
import { WelcomePage } from '../pages/welcome/welcome';
import { TabsPage } from '../pages/tabs/tabs';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    firebase.initializeApp({
      apiKey: "AIzaSyB6eTKNavkomPyG8FRsoLYdyDBBUSfuTLw",
      authDomain: "r-review-e3cf7.firebaseapp.com",
      databaseURL: "https://r-review-e3cf7.firebaseio.com",
      projectId: "r-review-e3cf7",
      storageBucket: "r-review-e3cf7.appspot.com",
      messagingSenderId: "593909550018"});

      const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
        if (!user) {
          this.rootPage = WelcomePage;
          unsubscribe();
        } else {
          this.rootPage = TabsPage;
          unsubscribe();
        }
      });
  }

  

  
}
